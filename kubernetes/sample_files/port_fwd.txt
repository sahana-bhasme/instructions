To test the cluster ip service, we can use port forwarding.
Use the following command to do the port forwarding.

kubectl port-forward svc/clusterip_service 8088:80

Test the above using:

curl localhost:8088